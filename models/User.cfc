component extends="quick.models.BaseEntity" accessors="true" {

    property name="id";
    property name="username";
    property name="email";
    property name="firstName" column="first_name";
    property name="lastName" column="last_name";
    property name="password";
    property name="createdDate" column="created_date";
    property name="city";
    property name="country";


    function newCollection( array entities = [] ) {
        return variables._wirebox.getInstance(
            name = "quick.extras.QuickCollection",
            initArguments = {
                "collection" = arguments.entities
            }
        );
    }

}