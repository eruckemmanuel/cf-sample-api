component {
    
    function up( schema, query ) {
        schema.create( "users", function( table ) {
            table.increments( "id" );
            table.string( "username").unique();
            table.string( "email" ).unique();
            table.string( "first_name" );
            table.string( "last_name" );
            table.string( "city" );
            table.string( "country" );
            table.string( "password" );
            table.timestamp( "created_date" );
        } );
    }
 
    function down( schema, query ) {
        schema.drop( "users" );
    }

}
