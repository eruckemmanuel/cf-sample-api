component {
    property name="userService" inject="quickService:User";
	
    any function index(event, rc, prc){
        var users = userService.all().$renderData();
        return users;
    }
 
	
    // POST /users
	
    function create( event, rc, prc ) {
	
        return userService.create( {
            "username": rc.username,
            "firstName": rc.firstName,
            "lastName": rc.lastName,
            "city": rc.city,
            "country": rc.country,
            "email": rc.email,
            "password": rc.password,
            "createdDate": now(),
	
        } );
	
    }


    // GET /users/:id
    function get(event, rc, prc){
        return userService.findOrFail(rc.id);
    }
	
 
	
    // PUT /users/:id
	
    function update( event, rc, prc ) {
        var user = userService.findOrFail( rc.id );
	
        return user.update( {
            "firstName": rc.firstName,
            "lastName": rc.lastName,
            "country": rc.country,
            "city": rc.city
        } );
	
	
    }

    // DELETE /users/:id
    function delete(event, rc, prc){
        var user = getInstance( "User" ).find( 1 );
        user.delete();
        return {
            "status": "Deleted"
        }
    }
}