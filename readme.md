# CF Sample API

Sample API with ColdFusion

Source Code

-   https://gitlab.com/eruckemmanuel/cf-sample-api

## Installation

```bash
cd cf-sample-api
```

```bash
box install
```

This will setup all the needed dependencies for application.

Run server

```bash
box server start --rewritesEnable
```
